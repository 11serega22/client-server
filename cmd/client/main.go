package main

import (
	"encoding/json"
	"flag"
	"github.com/gorilla/websocket"
	"gitlab.com/client-server/entity"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"
)

var addr = flag.String("addr", "localhost:8080", "http service address")
var message = flag.String("message", "", "message")

func main() {
	flag.Parse()
	log.SetFlags(0)

	if message == nil || *message == "" {
		log.Fatalf("message is required and it should be in right format, message - %v", message)
	}

	var messageStruct entity.Registration
	err := json.Unmarshal([]byte(*message), &messageStruct)
	if err != nil {
		log.Fatalf("message is not in correct format, message - %v, err - %v", message, err)
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "ws", Host: *addr, Path: "/ws"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer c.Close()

	done := make(chan struct{})

	friends := make(map[int]bool)
	for _, v := range messageStruct.Friends {
		friends[v] = false
	}

	go func() {
		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}
			var status []entity.StatusOnline
			if err := json.Unmarshal(message, &status); err != nil {
				log.Fatal("form data invalid:", err)
			}
			for _, v := range status {
				if _, ok := friends[v.ID]; !ok {
					continue
				}
				friends[v.ID] = v.Online
			}
			byteData, err := json.Marshal(friends)
			log.Printf(string(byteData))
		}
	}()

	err = c.WriteMessage(websocket.TextMessage, []byte(*message))
	if err != nil {
		log.Println("write:", err)
		return
	}

	for {
		select {
		case <-done:
			return
		case <-interrupt:
			log.Println("interrupt")
			// Cleanly close the connection by sending a close message and then
			// waiting (with timeout) for the server to close the connection.
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				log.Println("write close:", err)
				return
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}
