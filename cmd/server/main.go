package main

import (
	"github.com/jinzhu/configor"
	"github.com/sirupsen/logrus"
	"gitlab.com/client-server/api/websocket"
	"gitlab.com/client-server/config"
)

func main() {
	log := logrus.New()
	logRequest := log.WithFields(logrus.Fields{
		"service name": "chat",
	})
	cfg := config.Config{}
	if err := configor.New(&configor.Config{ENVPrefix: "WEB"}).Load(&cfg, "config/config.yaml"); err != nil {

		panic(err)
	}
	server := websocket.NewServer(cfg.Addr, logRequest)
	if err := server.Serve(); err != nil {
		logRequest.Fatal(err)
	}
}
