package integration

import (
	"bytes"
	"fmt"
	docker "github.com/fsouza/go-dockerclient"
	"gitlab.com/client-server/infra"
	"io"
	"log"
	"os/exec"
	"strings"
	"time"
)

type DockerManager struct {
	Client *docker.Client
}

func NewDockerManager() DockerManager {
	client, err := docker.NewClientFromEnv()
	if err != nil {
		panic(err)
	}
	return DockerManager{
		Client: client,
	}
}

type ContainerConditions struct {
	env     []string
	volume  []string
	cmd     []string
	port    map[docker.Port][]docker.PortBinding
	network *string
	image   *string
}

type ContainerFn func(*ContainerConditions)

func ContainerPort(ports map[string]string) ContainerFn {
	portBindings := make(map[docker.Port][]docker.PortBinding)
	for hostPort, dockerPort := range ports {
		portBindings[docker.Port(dockerPort+"/tcp")] = []docker.PortBinding{{
			HostPort: hostPort,
			HostIP:   "0.0.0.0",
		}}
	}
	return func(o *ContainerConditions) {
		o.port = portBindings
	}
}

func ContainerNetwork(network string) ContainerFn {
	return func(o *ContainerConditions) {
		o.network = &network
	}
}

func ContainerImage(image string) ContainerFn {
	return func(o *ContainerConditions) {
		o.image = &image
	}
}

func ContainerEnv(env []string) ContainerFn {
	return func(o *ContainerConditions) {
		o.env = env
	}
}

func ContainerVolume(volume []string) ContainerFn {
	return func(o *ContainerConditions) {
		o.volume = volume
	}
}

func ContainerCmd(cmd []string) ContainerFn {
	return func(o *ContainerConditions) {
		o.cmd = cmd
	}
}

func (d *DockerManager) CreateContainer(conditions ...ContainerFn) (*docker.Container, error) {
	cond := ContainerConditions{}
	for _, o := range conditions {
		o(&cond)
	}
	config := &docker.Config{}
	if cond.image != nil {
		config.Image = *cond.image
	}
	if len(cond.env) > 0 {
		config.Env = cond.env
	}
	if len(cond.cmd) > 0 {
		config.Cmd = cond.cmd
	}
	hostConfig := &docker.HostConfig{AutoRemove: true}
	if cond.network != nil {
		hostConfig.NetworkMode = *cond.network
	}
	if len(cond.port) > 0 {
		hostConfig.PortBindings = cond.port
	}
	if len(cond.volume) > 0 {
		hostConfig.Binds = cond.volume
	}
	container, err := d.Client.CreateContainer(docker.CreateContainerOptions{
		Config:     config,
		HostConfig: hostConfig,
	})
	if err != nil {
		return nil, err
	}

	if err = d.Client.StartContainer(container.ID, &docker.HostConfig{}); err != nil {
		return nil, err
	}
	return container, nil
}

func (d *DockerManager) LastLog(containerID string) string {
	var stdout, stderr bytes.Buffer
	cmd := exec.Command("docker", "logs", "-f", containerID)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	go func() {
		err := cmd.Start()
		if err != nil {
			log.Printf("error - %v\r\n", err)
			fmt.Printf("stdout - '%s'\r\n", stdout.String())
			fmt.Printf("stderr - '%s'\n\r", stderr.String())
		}
	}()
	time.Sleep(time.Millisecond * 150)
	lines := strings.Split(strings.Replace(stderr.String(), "\r\n", "\n", -1), "\n")
	return lines[len(lines)-2]
}

func (d *DockerManager) CreateImage(name, dockerfile, context string) error {
	var stdout, stderr bytes.Buffer
	cmd := exec.Command("docker", "build", "-f", dockerfile, "-t", name, context)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		log.Printf("error - %v\r\n", err)
		fmt.Printf("stdout - '%s'\r\n", stdout.String())
		fmt.Printf("stderr - '%s'\n\r", stderr.String())
	}
	return err
}

func (d *DockerManager) ExecDocker(dockerID string, stdin io.Reader, cmds ...string) error {
	var stdout, stderr bytes.Buffer
	cmd := exec.Command("docker", append([]string{"exec", "-i", dockerID}, cmds...)...)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	if stdin != nil {
		cmd.Stdin = stdin
	}
	err := cmd.Run()
	if err != nil {
		log.Printf("backup error - %v\r\n", err)
		fmt.Printf("stdout - '%s'\r\n", stdout.String())
		fmt.Printf("stderr - '%s'\n\r", stderr.String())
	}
	return err
}

func (d *DockerManager) GetNetwork(name string) *docker.Network {
	nets, err := d.Client.FilteredListNetworks(map[string]map[string]bool{"name": {name: true}})
	if err != nil {
		panic(err)
	}
	if len(nets) == 0 {
		return nil
	}
	return &nets[0]
}

func (d *DockerManager) CreateNetwork(typ, name string) *docker.Network {
	if typ == "" {
		typ = "bridge"
	}
	if name == "" {
		name = infra.GetGUID()
	}
	nets, err := d.Client.CreateNetwork(docker.CreateNetworkOptions{
		Driver: typ,
		Name:   name,
	})
	if err != nil {
		panic(err)
	}
	return nets
}

func (d *DockerManager) GetContainer(id string) *docker.Container {
	containers, err := d.Client.ListContainers(docker.ListContainersOptions{
		Filters: map[string][]string{"id": []string{id}},
	})
	if err != nil {
		panic(err)
	}
	if len(containers) == 0 {
		return nil
	}
	container, err := d.Client.InspectContainerWithOptions(docker.InspectContainerOptions{
		ID: containers[0].ID,
	})
	if err != nil {
		panic(err)
	}
	return container
}
