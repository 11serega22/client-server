//+build integration

package integration

import (
	"encoding/json"
	"fmt"
	docker "github.com/fsouza/go-dockerclient"
	"gitlab.com/client-server/entity"
	"testing"
	"time"
)

func TestIntegration(t *testing.T) {
	test := true
	serverIP := "172.18.0.6:8080"
	dm := NewDockerManager()
	network := dm.CreateNetwork("", "")
	var containers []*docker.Container
	defer func() {
		for _, v := range containers {
			_ = dm.Client.RemoveContainer(docker.RemoveContainerOptions{
				ID:    v.ID,
				Force: true,
			})
		}
		_ = dm.Client.RemoveNetwork(network.ID)
		fmt.Println("container deleted")
	}()
	if test {
		//creating image for project
		createImage := func(imageName, dockerfile string) {
			err := dm.CreateImage(imageName, dockerfile, "..")
			if err != nil {
				t.Errorf("build image error - %v\r\n", err)
				return
			}
			fmt.Println("image created")
		}

		createImage("server-ws", "../deployment/DockerfileServer")
		createImage("client-ws", "../deployment/DockerfileClient")

		//creating and launching container of project
		server, err := dm.CreateContainer(
			ContainerImage("server-ws"),
			ContainerNetwork(network.Name),
			ContainerPort(map[string]string{"8080": "8080"}),
		)
		if err != nil {
			t.Errorf("launching conteiner error - %v\r\n", err)
			return
		}
		containers = append(containers, server)
		fmt.Printf("server launched\r\n")
		time.Sleep(time.Second * 2)
		server = dm.GetContainer(server.ID)
		serverIP = server.NetworkSettings.Networks[network.Name].IPAddress + ":8080"
		fmt.Println(serverIP)
	}

	testData := []struct {
		message     entity.Registration
		containerID string
		comment     map[int]string
	}{
		{
			message: entity.Registration{
				UserID:  1,
				Friends: []int{2, 3, 4},
			},
			comment: map[int]string{0: `{"2":false,"3":false,"4":false}`},
		},
		{
			message: entity.Registration{
				UserID:  2,
				Friends: []int{1, 3, 4},
			},
			comment: map[int]string{
				0: `{"2":true,"3":false,"4":false}`,
				1: `{"1":true,"3":false,"4":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  1,
				Friends: []int{2, 3},
			},
			comment: map[int]string{
				0: `{"2":true,"3":false,"4":false}`,
				1: `{"1":true,"3":false,"4":false}`,
				2: `{"2":true,"3":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  3,
				Friends: []int{2, 1, 4},
			},
			comment: map[int]string{
				0: `{"2":true,"3":true,"4":false}`,
				1: `{"1":true,"3":true,"4":false}`,
				2: `{"2":true,"3":true}`,
				3: `{"1":true,"2":true,"4":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  4,
				Friends: []int{2, 3, 1},
			},
			comment: map[int]string{
				0: `{"2":true,"3":true,"4":false}`,
				1: `{"1":true,"3":true,"4":true}`,
				2: `{"2":true,"3":true}`,
				3: `{"1":true,"2":true,"4":true}`,
				4: `{"1":true,"2":true,"3":true}`,
			},
		},
	}

	for k, v := range testData {
		byteData, err := json.Marshal(v.message)
		if err != nil {
			t.Errorf("json Marshal error - %v\r\n", err)
			return
		}
		client, err := dm.CreateContainer(
			ContainerImage("client-ws"),
			ContainerNetwork(network.Name),
			ContainerCmd([]string{"/root/app", "-message", string(byteData), "-addr", serverIP}),
		)
		if err != nil {
			t.Errorf("launching conteiner error - %v\r\n", err)
			return
		}
		testData[k].containerID = client.ID
		containers = append(containers, client)
		for containerID, testMessage := range v.comment {
			log := dm.LastLog(testData[containerID].containerID)
			if testMessage != log {
				t.Errorf("recived unexpected message. Expected - '%s' Got - '%s'\r\n", testMessage, log)
			}
		}
	}
	testData[0].comment = map[int]string{
		1: `{"1":true,"3":true,"4":true}`,
		2: `{"2":true,"3":true}`,
		3: `{"1":true,"2":true,"4":true}`,
		4: `{"1":true,"2":true,"3":true}`,
	}
	testData[1].comment = map[int]string{
		2: `{"2":false,"3":true}`,
		3: `{"1":true,"2":false,"4":true}`,
		4: `{"1":true,"2":false,"3":true}`,
	}
	testData[2].comment = map[int]string{
		3: `{"1":false,"2":false,"4":true}`,
		4: `{"1":false,"2":false,"3":true}`,
	}
	testData[3].comment = map[int]string{
		4: `{"1":false,"2":false,"3":false}`,
	}

	for k, v := range testData {
		if k > 3 {
			break
		}
		if err := dm.Client.RemoveContainer(docker.RemoveContainerOptions{
			ID:    v.containerID,
			Force: true,
		}); err != nil {
			t.Errorf("unexpected error - %v\r\n", err)
		}

		for containerID, testMessage := range v.comment {
			log := dm.LastLog(testData[containerID].containerID)
			if testMessage != log {
				t.Errorf("recived unexpected message. Expected - '%s' Got - '%s'\r\n", testMessage, log)
			}
		}
	}
}
