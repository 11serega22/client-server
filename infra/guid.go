package infra

import (
	uuid "github.com/satori/go.uuid"
	"strings"
)

func GetGUID() string {
	guid := uuid.NewV4()
	return strings.ToUpper(strings.ReplaceAll(guid.String(), "-", ""))
}
