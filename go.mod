module gitlab.com/client-server

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/fsouza/go-dockerclient v1.6.2
	github.com/gorilla/websocket v1.4.1
	github.com/jinzhu/configor v1.1.1
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
)
