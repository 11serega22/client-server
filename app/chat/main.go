package chat

import (
	"gitlab.com/client-server/entity"
	"sync"
	"sync/atomic"
)

type User struct {
	key uint64
	id  int
	ch  chan entity.StatusOnline
}

func (u *User) Chan() chan entity.StatusOnline {
	return u.ch
}

type Chat struct {
	session     map[int]map[uint64]User
	subscribers map[int]map[int]struct{}
	fiends      map[int][]int
	key         uint64
	lk          sync.RWMutex
}

func NewChat() Chat {
	return Chat{
		session:     make(map[int]map[uint64]User),
		subscribers: make(map[int]map[int]struct{}),
		fiends:      make(map[int][]int),
	}
}

func (c *Chat) NewSession(id int, friends []int) User {
	atomic.AddUint64(&c.key, 1)
	c.lk.Lock()
	defer c.lk.Unlock()
	if !equalSlice(c.fiends[id], friends) {
		//deleting previous subscriptions
		if fiends1, ok := c.fiends[id]; ok {
			for _, friend := range fiends1 {
				if _, ok := c.subscribers[friend]; !ok {
					continue
				}
				delete(c.subscribers[friend], id)
			}
		}
		//creating new subscription
		for _, friend := range friends {
			if _, ok := c.subscribers[friend]; !ok {
				c.subscribers[friend] = make(map[int]struct{})
			}
			c.subscribers[friend][id] = struct{}{}
		}
		c.fiends[id] = friends
	}
	//checking maybe user already online
	if _, ok := c.session[id]; !ok {
		c.sendToSubscribers(id, true)
	}
	//creating new session for user
	if _, ok := c.session[id]; !ok {
		c.session[id] = make(map[uint64]User)
	}
	c.session[id][c.key] = User{
		key: c.key,
		id:  id,
		ch:  make(chan entity.StatusOnline),
	}
	return c.session[id][c.key]
}

func equalSlice(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func (c *Chat) sendToSubscribers(id int, status bool) {
	//check if anyone is following this user
	if _, ok := c.subscribers[id]; !ok {
		return
	}
	for subscriber, _ := range c.subscribers[id] {
		//check if the subscriber is online, if yes, then send a message to all his active sessions
		if sessions, ok := c.session[subscriber]; ok {
			for _, session := range sessions {
				select {
				case session.ch <- entity.StatusOnline{
					ID:     id,
					Online: status,
				}:
				default:
				}

			}
		}
	}
}

func (c *Chat) DeleteSession(user User) {
	c.lk.Lock()
	defer c.lk.Unlock()
	if _, ok := c.session[user.id]; ok {
		delete(c.session[user.id], user.key)
		if len(c.session[user.id]) == 0 {
			delete(c.session, user.id)
			c.sendToSubscribers(user.id, false)
		}
	}
}

func (c *Chat) SessionsStatus(id int) []entity.StatusOnline {
	c.lk.RLock()
	defer c.lk.RUnlock()
	statusOnline := make([]entity.StatusOnline, len(c.fiends[id]))
	for k, friend := range c.fiends[id] {
		statusOnline[k].ID = friend
		if _, ok := c.session[friend]; ok {
			statusOnline[k].Online = true
		}
	}
	return statusOnline
}
