package chat

import (
	"encoding/json"
	"gitlab.com/client-server/entity"
	"testing"
)

func TestChatConnection(t *testing.T) {
	chat := NewChat()
	testData := []struct {
		message entity.Registration
		user    User
		comment map[int]string
	}{
		{
			message: entity.Registration{
				UserID:  1,
				Friends: []int{2, 3, 4},
			},
			comment: map[int]string{0: `{"2":false,"3":false,"4":false}`},
		},
		{
			message: entity.Registration{
				UserID:  2,
				Friends: []int{1, 3, 4},
			},
			comment: map[int]string{
				0: `{"2":true,"3":false,"4":false}`,
				1: `{"1":true,"3":false,"4":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  1,
				Friends: []int{2, 3},
			},
			comment: map[int]string{
				0: `{"2":true,"3":false}`,
				1: `{"1":true,"3":false,"4":false}`,
				2: `{"2":true,"3":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  3,
				Friends: []int{2, 1, 4},
			},
			comment: map[int]string{
				0: `{"2":true,"3":true}`,
				1: `{"1":true,"3":true,"4":false}`,
				2: `{"2":true,"3":true}`,
				3: `{"1":true,"2":true,"4":false}`,
			},
		},
		{
			message: entity.Registration{
				UserID:  4,
				Friends: []int{2, 3, 1},
			},
			comment: map[int]string{
				0: `{"2":true,"3":true}`,
				1: `{"1":true,"3":true,"4":true}`,
				2: `{"2":true,"3":true}`,
				3: `{"1":true,"2":true,"4":true}`,
				4: `{"1":true,"2":true,"3":true}`,
			},
		},
	}

	for k, v := range testData {
		user := chat.NewSession(v.message.UserID, v.message.Friends)
		testData[k].user = user
		for u, testMessage := range v.comment {
			friends := chat.SessionsStatus(testData[u].user.id)
			stringFriends := getStringFromStatusOnline(friends)
			if testMessage != stringFriends {
				t.Errorf("recived unexpected message. Expected - '%s' Got - '%s'\r\n", testMessage, stringFriends)
			}
		}
	}
	testData[0].comment = map[int]string{
		1: `{"1":true,"3":true,"4":true}`,
		2: `{"2":true,"3":true}`,
		3: `{"1":true,"2":true,"4":true}`,
		4: `{"1":true,"2":true,"3":true}`,
	}
	testData[1].comment = map[int]string{
		2: `{"2":false,"3":true}`,
		3: `{"1":true,"2":false,"4":true}`,
		4: `{"1":true,"2":false,"3":true}`,
	}
	testData[2].comment = map[int]string{
		3: `{"1":false,"2":false,"4":true}`,
		4: `{"1":false,"2":false,"3":true}`,
	}
	testData[3].comment = map[int]string{
		4: `{"1":false,"2":false,"3":false}`,
	}

	for k, v := range testData {
		if k > 3 {
			break
		}
		chat.DeleteSession(v.user)
		for u, testMessage := range v.comment {
			friends := chat.SessionsStatus(testData[u].user.id)
			stringFriends := getStringFromStatusOnline(friends)
			if testMessage != stringFriends {
				t.Errorf("recived unexpected message. Expected - '%s' Got - '%s'\r\n", testMessage, stringFriends)
			}
		}
	}
}

func getStringFromStatusOnline(en []entity.StatusOnline) string  {
	enMap := map[int]bool{}
	for _, v := range en {
		enMap[v.ID] = v.Online
	}
	byteData, err := json.Marshal(enMap)
	if err != nil {
		panic(err)
	}
	return string(byteData)
}
