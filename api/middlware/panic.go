package middlware

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"regexp"
	"runtime/debug"
)

type Middleware struct{
	log *logrus.Entry
}

func NewMiddleware(log *logrus.Entry) Middleware {
	return Middleware{log: log}
}

var re = regexp.MustCompile(`\n`)

type HttpFunc func(w http.ResponseWriter, r *http.Request)

func (m *Middleware) Panic(next HttpFunc) HttpFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				err, ok := err.(error)
				if ok {
					stacktrace := string(debug.Stack())
					stacktrace = re.ReplaceAllString(stacktrace, `\n`)
					m.log.WithField("where", stacktrace).Fatal(err)
				}
			}
		}()
		next(w, r)
	}
}
