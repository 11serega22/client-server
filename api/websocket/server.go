package websocket

import (
	"encoding/json"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"gitlab.com/client-server/api"
	"gitlab.com/client-server/api/middlware"
	chat2 "gitlab.com/client-server/app/chat"
	"gitlab.com/client-server/entity"
	"net/http"
)

type Server interface {
	Serve() error
}

type server struct {
	addr string
	chat chat2.Chat
	log  *logrus.Entry
}

func validationAdd(data []byte) (entity.Registration, error) {
	post := entity.Registration{}
	if err := json.Unmarshal(data, &post); err != nil {
		return post, err
	}
	if _, err := govalidator.ValidateStruct(post); err != nil {
		return post, err
	}
	return post, nil
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
}

func (s *server) register(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		api.WriteError(api.CodeUnexpected, err.Error(), w)
		s.log.Warnf("connection interrupted - %v", err)
		return
	}
	defer ws.Close()
	user := chat2.User{}
	stdoutDone := make(chan bool)
	waitRead := make(chan bool)
	go func() {
		<-waitRead
		for {
			select {
			case status := <-user.Chan():
				if err := ws.WriteMessage(websocket.TextMessage, getByteFromStatusOnline(status)); err != nil {
					s.log.Warnf("sending message to client err - %v", err)
				}
			case <-stdoutDone:
				return
			}
		}
	}()
	for {
		_, body, err := ws.ReadMessage()
		if err != nil {
			stdoutDone <- true
			s.chat.DeleteSession(user)
			break
		}
		req, err := validationAdd(body)
		if err != nil {
			api.WriteError(api.CodeDataInvalid, err.Error(), w)
			s.log.Warnf("request data is invalid. err - %v", err)
			return
		}
		user = s.chat.NewSession(req.UserID, req.Friends)
		status := s.chat.SessionsStatus(req.UserID)
		if err := ws.WriteMessage(websocket.TextMessage, getByteFromStatusOnline(status...)); err != nil {
			s.log.Warnf("sending message to client err - %v", err)
		}
		select {
		case waitRead <- true:
		default:
		}
	}
}

func getByteFromStatusOnline(so ...entity.StatusOnline) []byte {
	byteData, err := json.Marshal(so)
	if err != nil {
		panic(err)
	}
	return byteData
}

func (s *server) Serve() error {
	mid := middlware.NewMiddleware(s.log)
	router := http.NewServeMux()
	router.HandleFunc("/ws", mid.Panic(s.register))
	return http.ListenAndServe(s.addr, router)
}

func NewServer(addr string, log *logrus.Entry) Server {
	return &server{
		addr: addr,
		log:  log,
		chat: chat2.NewChat(),
	}
}
