package entity

type StatusOnline struct {
	ID     int
	Online bool
}
