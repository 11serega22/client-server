package entity

type Registration struct {
	UserID  int   `json:"user_id"`
	Friends []int `json:"friends"`
}
